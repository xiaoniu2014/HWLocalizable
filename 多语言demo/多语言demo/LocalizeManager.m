//
//  LocalizeManager.m
//  多语言demo
//
//  Created by 洪伟 on 16/6/8.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import "LocalizeManager.h"

@implementation LocalizeManager


+ (instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static LocalizeManager *_sharedManager;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[LocalizeManager alloc] init];
        _sharedManager.language = @"en";
    });
    return _sharedManager;
}

+ (NSString *)localizedStringForKey:(NSString *)key{
    NSString *path = [[NSBundle mainBundle] pathForResource:[[self sharedManager] language] ofType:@"lproj"];
    return [[NSBundle bundleWithPath:path] localizedStringForKey:key value:nil table:@"HWLocalizable"];
}

@end
