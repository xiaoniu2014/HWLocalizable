//
//  ViewController.m
//  多语言demo
//
//  Created by 洪伟 on 16/6/8.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import "ViewController.h"
#import "LocalizeManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)buttonClick:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
            [LocalizeManager sharedManager].language = @"en";
            break;
        case 1:
            [LocalizeManager sharedManager].language = @"zh-Hans";
            break;
        case 2:
            [LocalizeManager sharedManager].language = @"zh-Hant";
            break;
        default:
            break;
    }
    
    NSString *str = [LocalizeManager localizedStringForKey:@"showText"];
    NSLog(@"===%@",str);
}


@end
