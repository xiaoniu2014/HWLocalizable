//
//  LocalizeManager.h
//  多语言demo
//
//  Created by 洪伟 on 16/6/8.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalizeManager : NSObject


@property (copy, nonatomic) NSString *language;
+ (instancetype)sharedManager;
+ (NSString *)localizedStringForKey:(NSString *)key;
@end
