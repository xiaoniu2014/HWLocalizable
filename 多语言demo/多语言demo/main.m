//
//  main.m
//  多语言demo
//
//  Created by 洪伟 on 16/6/8.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
